def factorial_1(number):

    if(type(number)!=int):
        raise ValueError('Invalid Input')

    try:
        fact=1
        while(number>1):
            fact = fact * number
            number -= 1
    except:
        raise 

    return fact


def factorial_2(number):
    if(type(number)!=int):
        raise ValueError('Invalid Input')
    
    return 1 if (number<1) else number * factorial(number -1)


import math

def factorial_3(number):
    if(type(number)!=int):
        raise ValueError('Invalid Input')
    return 1 if (number<1) else math.factorial(number)
